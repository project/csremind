Overview:

This module works with the 'csremind' (Contact Save Remind) module to implement
functionality to remind a user that there are unread saved contact form
messages.

If there are unread messages, the user will see a Drupal message, every X period
of time, presenting them with links to the list of saved contact form messages,
a link enabling them to permanently deactivate the reminder messages, and a link
that will suppress the reminder messages for the remainder of their current
session.

The configuration of a users reminders can be done via a 'tab' accessible from
their user profile (directly accessible at /user/<uid>/reminders). The
configuration form there enables the user to (de)activate/(un)supress reminders
and also alter the frequency that the reminder is shown.

Theming:

You can implement a theme override for the reminder message shown to the user by
adding a function named in the following format, to your themes' template.php:

  YOURTHEMENAME_csremind_message_reminder_message()

The simplest way to do this is to copy the entire
"theme_csremind_message_reminder_message()" function from the .module file,
paste it into your themes' template.php file, alter the name (as above) and
alter the content.

You can use the above technique to also override the theming of the AJAX
messages shown to the user when they click the "Deactivate" and "Suppress"
reminder message links with:

  YOURTHEMENAME_csremind_message_deactivated_message()

  and

  YOUTHEMENAME_csremind_message_suppressed_message()

All theming of the reminder message is left to the site builder. You can inspect
the reminder message default markup to see that there are a number of CSS
classes available to use in your chosen theme.